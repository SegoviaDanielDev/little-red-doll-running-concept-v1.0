﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class MobilePlatform : MonoBehaviour
{

    [SerializeField] private Transform target;
    [SerializeField] private float velocity;

    private Vector3 start, end;
    void Start()
    {
        if(target != null)
        {
            target.parent = null;
            start = transform.position;
            end = target.position;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, (velocity * Time.deltaTime));
        }

        if(transform.position == target.position)
        {
            target.position = (target.position == start) ? end : start;
        }
    }
}
