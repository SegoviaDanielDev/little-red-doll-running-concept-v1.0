﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEditor;
using UnityEngine;

public class BoostAndDecrease : MonoBehaviour
{
    private PlayerController player;
    private AudioSource AS;
    [SerializeField] private float Speed;

    public void Start()
    {
        AS = GetComponent<AudioSource>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
       

        if(player != null && gameObject.tag == "Boost")
        {
            player.maxSpeed += Speed;
            AS.Play();
        }

        else if(player != null)
        {
            player.maxSpeed -= Speed;
            AS.Play();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
       
        
        player = collision.GetComponent<PlayerController>();

        if(player != null)
        player.maxSpeed = 12f;
    }




}
