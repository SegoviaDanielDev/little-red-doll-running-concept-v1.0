﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private string gameScene;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject howMenu;

    [SerializeField] private Button playButton;
    [SerializeField] private Button howButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button howBackButton;

    private AudioSource AS;


    private void Start()
    {
        AS = gameObject.GetComponent<AudioSource>();
    }
    private void Awake()
    {

        playButton.onClick.AddListener(OnClickPlay);
        howButton.onClick.AddListener(OnClickHow);
        quitButton.onClick.AddListener(OnClickQuit);
        howBackButton.onClick.AddListener(OnClickHowBack);

        ShowMainMenu();

    }

    private void OnClickPlay()
    {
        AS.Stop();
        AS.Play();
        SceneManager.LoadScene(gameScene);

    }

    private void OnClickHow()
    {
        AS.Play();
        ShowHowMenu();

    }

    private void OnClickHowBack()
    {
        AS.Play();
        ShowMainMenu();
    }

    private void ShowMainMenu()
    {
        mainMenu.SetActive(true);
        howMenu.SetActive(false);
    }

    private void ShowHowMenu()
    {
        mainMenu.SetActive(false);
        howMenu.SetActive(true);
    }

    private void OnClickQuit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#endif
        AS.Stop();
        AS.Play();
        Application.Quit();
    }

}